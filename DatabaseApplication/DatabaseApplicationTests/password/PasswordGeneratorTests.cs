﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseApplication.password;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.password.Tests
{
    [TestClass()]
    public class PasswordGeneratorTests
    {
        [TestMethod()]
        public void PasswordGeneratorTest()
        {
            PasswordGenerator gen = new CapitalLettersPasswordGeneratorDecorator(new PasswordGenerator(10));
            var s = gen.Generate();
            Assert.AreEqual(s.Length, 10);
            Assert.IsTrue(s.All(char.IsUpper));
            gen = new DigitsPasswordGeneratorDecorator(gen);
            s = gen.Generate();
            Assert.AreEqual(s.Length, 10);
            Assert.IsTrue(s.All(c => char.IsUpper(c) || char.IsDigit(c)));
            s=new SmallLettersPasswordGeneratorDecorator(new SymbolsPasswordGeneratorDecorator(
                new PasswordGenerator(15))).Generate();
            Assert.AreEqual(s.Length, 15);
            Assert.IsTrue(s.All(c => char.IsLower(c) || "!@#$%^&*()-_+=[];'\"\\/:,.".Contains(c)));
        }

        [TestMethod()]
        public void GenerateTest()
        {
            var gen = new PasswordGenerator(10);
            Assert.AreEqual(gen.Generate(),"");
            gen = new CapitalLettersPasswordGeneratorDecorator(new PasswordGenerator(0));
            Assert.AreEqual(gen.Generate(), "");
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void NullGenTest()
        {
            PasswordGenerator p=null;
            new CapitalLettersPasswordGeneratorDecorator(p);
            
        }
    }
}