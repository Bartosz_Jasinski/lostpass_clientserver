﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

namespace DatabaseApplication.crypto.Tests
{
    [TestClass()]
    public class AesEncryptorTests
    {
        private string randomString(int length = 10)
        {
            StringBuilder sb = new StringBuilder();
            Random rand = new Random();
            for (int i = 0; i < length; i++)
            {
                sb.Append((char) rand.Next(256));
            }
            return sb.ToString();
        }

        private SecureString getSS(string s)
        {
            SecureString ss = new SecureString();
            foreach (var c in s)
            {
                ss.AppendChar(c);
            }
            return ss;
        }

        [TestMethod()]
        public void AES_EncryptTest()
        {
            AesEncryptor enc = new AesEncryptor();
            var s = randomString();
            var pass = randomString();
            var res = enc.DecryptText(enc.EncryptText(s, pass), pass);
            Assert.AreEqual(s, res);
        }

        [TestMethod]
        public void UnicodeEncryptTest()
        {
            AesEncryptor enc = new AesEncryptor();
            var s = "Секретный текст";
            var pass = randomString();
            var res = enc.DecryptText(enc.EncryptText(s, pass), pass);
            Assert.AreEqual(s, res);
        }
   

        [TestMethod]
        public void NullEncryptTest()
        {
            AesEncryptor enc = new AesEncryptor();
            Assert.AreEqual(enc.EncryptText(null, ""), "");
            Assert.AreEqual(enc.EncryptText(null, "sadd"), "");
            Assert.AreEqual(enc.EncryptText("", ""), "");
            Assert.AreEqual(enc.EncryptText("", null), "");
            Assert.AreEqual(enc.EncryptText("sda", null), "");
            Assert.AreEqual(enc.EncryptText("sda", ""), "");
            Assert.AreEqual(enc.DecryptText("sda", ""), "");
            Assert.AreEqual(enc.DecryptText(null, "asd"), "");
        }



    }
}