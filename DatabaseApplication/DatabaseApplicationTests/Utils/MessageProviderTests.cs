﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseApplication.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.Utils.Tests
{
    [TestClass()]
    public class MessageProviderTests
    {
        private MessageProvider mp;
        [TestInitialize]
        public void InitTests()
        {
            mp = MessageProvider.Instance;
        }
        [TestMethod()]
        public void SendMessageTest()
        {
            string s = "";
            string msg = "Message";
            mp.OnReceived += message => s = message.ToString();
            mp.SendMessage(new Message(msg));
            Assert.AreEqual(s, msg);
        }

        [TestMethod()]
        public void ReadLastMessageTest()
        {
            mp.SendMessage(new Message("AAA"));
            Assert.AreEqual(mp.ReadLastMessage().ToString(), "AAA");
        }

        [TestMethod()]
        public void ClearAllMessagesTest()
        {
            mp.SendMessage("BBB");
            mp.ClearAllMessages();
            Assert.IsTrue(mp.Messages.Count == 0);
        }
        [TestMethod(), ExpectedException(typeof(NullReferenceException))]
        public void NullMessageTestTest()
        {
            Message m = null;
            mp.SendMessage(m);
            mp.ClearAllMessages();
            Assert.IsTrue(mp.Messages.Count == 0);
        }

        [TestMethod()]
        public void LastMessageTypeTest()
        {
            mp.ClearAllMessages();
            Assert.IsNull(mp.LastMessageType());
        }

        [TestMethod()]
        public void ReadLastMessageTest1()
        {
            mp.ClearAllMessages();
            Assert.IsNull(mp.ReadLastMessage());
        }
    }
}