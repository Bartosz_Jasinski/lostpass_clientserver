﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LostPassServer.Data;
using LostPassServer.Models;
using Microsoft.AspNet.Identity;

namespace LostPassServer.Controllers
{
    [Authorize]
    public class PasswordsController : ApiController
    {
        private dotNETDBEntities db = new dotNETDBEntities();

        // GET: api/Passwords
        public IQueryable<Passwords> GetPasswords()
        {
            return db.Passwords;
        }

        // GET: api/Passwords/5
        [ResponseType(typeof(PasswordsDTO))]
        public async Task<IHttpActionResult> GetPasswords(int id)
        {
            Passwords passwords = await db.Passwords.Include("Pages").FirstOrDefaultAsync(p => p.PasswordID == id);
            if (passwords == null || passwords.Pages.Username != User.Identity.GetUserId())
            {
                return NotFound();
            }

            return Ok(new PasswordsDTO()
            {
                Login = passwords.UserLogin,
                Password = passwords.Pass,
                PasswordID = passwords.PasswordID
            });
        }

        // PUT: api/Passwords/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPasswords(int id, PasswordsDTO passwords)
        {
           if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != passwords.PasswordID)
            {
                return BadRequest();
            }


            try
            {
                
                var password = await db.Passwords.Include("Pages").FirstOrDefaultAsync(p => p.PasswordID == id);
                if (password == null || password.Pages.Username != User.Identity.GetUserId())
                    return NotFound();
                password.Pass = passwords.Password;
                password.UserLogin = passwords.Login;
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PasswordsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Passwords
        [ResponseType(typeof(Passwords))]
        public async Task<IHttpActionResult> PostPasswords(Passwords passwords)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Passwords.Add(passwords);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = passwords.PasswordID }, passwords);
        }

        // DELETE: api/Passwords/5
        [ResponseType(typeof(Passwords))]
        public async Task<IHttpActionResult> DeletePasswords(int id)
        {
            Passwords passwords = await db.Passwords.FindAsync(id);
            if (passwords == null)
            {
                return NotFound();
            }
            Pages page = await db.Pages.Include("Passwords").FirstOrDefaultAsync(p => p.PageID == passwords.PageID);

            db.Passwords.Remove(passwords);
            if (page.Passwords.Count == 0)
            {
                db.Pages.Remove(page);
            }
            await db.SaveChangesAsync();

            return Ok(passwords);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PasswordsExists(int id)
        {
            return db.Passwords.Count(e => e.PasswordID == id) > 0;
        }
    }
}