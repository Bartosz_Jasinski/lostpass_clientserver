﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using LostPassServer.Data;
using Microsoft.AspNet.Identity;

namespace LostPassServer.Controllers
{
    [Authorize]
    public class ConfigsController : ApiController
    {
        private dotNETDBEntities db = new dotNETDBEntities();

        // GET: api/Configs
        public async Task<IHttpActionResult> GetConfigs()
        {
            var name = User.Identity.GetUserId();
            var configs = await db.Configs.FindAsync(name);
            if (configs == null)
            {
                configs = new Configs()
                {
                    Username = name,
                    SortOrder = "Name - ascending"
                };
                db.Configs.Add(configs);
                await db.SaveChangesAsync();
            }
            return Ok(configs);
        }

        // GET: api/Configs/5
        [ResponseType(typeof(Configs))]
        public async Task<IHttpActionResult> GetConfigs(string id)
        {
            Configs configs = await db.Configs.FindAsync(id);
            if (configs == null)
            {
                return NotFound();
            }

            return Ok(configs);
        }

        // PUT: api/Configs/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutConfigs(string id, Configs configs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != configs.Username || id != User.Identity.GetUserId())
            {
                return BadRequest();
            }
            
            db.Entry(configs).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConfigsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Configs
        [ResponseType(typeof(Configs))]
        public async Task<IHttpActionResult> PostConfigs(Configs configs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
           // await DeleteConfigs(configs.Username);
            db.Configs.Add(configs);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ConfigsExists(configs.Username))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = configs.Username }, configs);
        }

        // DELETE: api/Configs/5
        [ResponseType(typeof(Configs))]
        public async Task<IHttpActionResult> DeleteConfigs(string id)
        {
            Configs configs = await db.Configs.FindAsync(id);
            if (configs == null)
            {
                return NotFound();
            }

            db.Configs.Remove(configs);
            await db.SaveChangesAsync();

            return Ok(configs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConfigsExists(string id)
        {
            return db.Configs.Count(e => e.Username == id) > 0;
        }
    }
}