﻿using System.Runtime.Serialization;

namespace LostPassServer.Models
{
    [DataContract]
    public class PasswordsDTO
    {
        [DataMember]
        public int PasswordID { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Password { get; set; }
    }
}