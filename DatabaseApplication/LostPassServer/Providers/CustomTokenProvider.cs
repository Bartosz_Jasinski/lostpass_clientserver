﻿using System;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using LostPassServer.Models;
using Microsoft.AspNet.Identity;

namespace LostPassServer.Providers
{
    //public static class CustomTokenProvider
    //{
    //    private static Dictionary<string, string> ShorterTokens = new Dictionary<string, string>();

    //    public static string GetShorterCode(string code, int length=6)
    //    {
    //        var s = "";

    //        do
    //        {
    //            s = RandomString(length);
    //        } while (ShorterTokens.ContainsKey(s));
    //            ShorterTokens[s] = code;
    //        return s;
    //    }

    //    private static string RandomString(int length)
    //    {
    //        var random = new Random();
    //        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    //        return new string(Enumerable.Repeat(chars, length)
    //          .Select(s => s[random.Next(s.Length)]).ToArray());
    //    }

    //    public static string ExtractCode(string s)
    //    {
    //        if (!ShorterTokens.ContainsKey(s))
    //        {
    //            return null;
    //        }
    //        var code = ShorterTokens[s];
    //        ShorterTokens.Remove(s);
    //        return code;
    //    }
    //}
    public class CustomTokenProvider : IUserTokenProvider<ApplicationUser, string>
    {
        public async Task<string> GenerateAsync(string purpose, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            var s = RandomString(6);
            user.EmailConfirmationToken = s;
            await manager.UpdateAsync(user);
            return await Task.FromResult(s);
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            return Task.FromResult(user.EmailConfirmationToken == token);
        }

        public Task NotifyAsync(string token, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            throw new NotImplementedException();
        }
        private static string RandomString(int length)
        {
            var random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }

}