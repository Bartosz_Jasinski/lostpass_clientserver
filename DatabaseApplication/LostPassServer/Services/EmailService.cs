﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using SendGrid;

namespace LostPassServer.Services
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            await ConfigSendGridAsync(message);
        }

        // Use NuGet to install SendGrid (Basic C# client lib) 
        private async Task ConfigSendGridAsync(IdentityMessage message)
        {
            var myMessage = new SendGridMessage();

            myMessage.AddTo(message.Destination);
            myMessage.From = new System.Net.Mail.MailAddress("noreply@lostpass.com", "LostPass support");
            myMessage.Subject = message.Subject;
            //myMessage.Html = GetHtml(message.Body);
            
            myMessage.Text = "Confirmation code";
            myMessage.Html = @"<p style=""font-size: 1.5em;		
		margin: 20;
		"">Here is your code</p> <p style=""font-size: 2em;
		color: #039BE5;
		margin: 0;
		text-align: center;"">" + message.Body + "</p>";

            //var credentials = new NetworkCredential(ConfigurationManager.AppSettings["emailService:Account"],
            //    ConfigurationManager.AppSettings["emailService:Password"]);

            // Create a Web transport for sending email.
            var transportWeb = new Web("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");

            // Send the email.
            if (transportWeb != null)
            {
                await transportWeb.DeliverAsync(myMessage);
            }
            else
            {
                //Trace.TraceError("Failed to create Web transport.");
                await Task.FromResult(0);
            }
        }


    }
}