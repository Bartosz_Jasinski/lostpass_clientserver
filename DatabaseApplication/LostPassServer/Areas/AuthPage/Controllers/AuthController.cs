﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using LostPassServer.Areas.AuthPage.Models;

namespace LostPassServer.Areas.AuthPage.Controllers
{
    public class AuthController : Controller
    {
        public AuthController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public AuthController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; set; }

        //// GET: AuthPage/Auth
        //public ActionResult Index()
        //{
        //    return View(Configuration.Services.GetApiExplorer().ApiDescriptions);
        //}

        public ActionResult Login([Bind(Include = "Email,Password")] LoginModel lm)
        {
            return View(lm);
        }
        public ActionResult Register([Bind(Include = "Email,Password,ConfirmPassword")] RegisterModel rm)
        {
            return View(rm);
        }

        public ActionResult Reset()
        {
            return View();
        }

        public ActionResult Forgot()
        {
            return View();
        }
    }
}