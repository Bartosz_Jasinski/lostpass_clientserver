﻿$(document).ready(function () {
    var token = localStorage.getItem("tokenInfo");
    var headers = {};
    if (token) {
        headers.Authorization = 'Bearer ' + token;
    }

    $.ajax({
        type: 'GET',
        url: '/api/pages',
        headers: headers
    }).success(function (data) {
        for (var t in JSON.parse(data)) {
            $(".table").add("<tr><td>" +
                t.Name +
                "</td>+<td>" +
                t.Passwords[0].UserLogin +
                "</td><td>" +
                t.Passwords[0].Pass +
                "</td></tr>")
        }


    }).fail(function (jqXHR, textStatus, error) {
        var response = JSON.parse(jqXHR.responseText);

        alert('Error: ' + error + " " + "\n" + textStatus.error + "\n" + response);
    });
})