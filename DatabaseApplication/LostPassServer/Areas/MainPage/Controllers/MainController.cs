﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;
using LostPassServer.Data;

namespace LostPassServer.Areas.MainPage.Controllers
{
    public class MainController : Controller
    {

        public MainController()
            : this(GlobalConfiguration.Configuration)
        {
        }

        public MainController(HttpConfiguration config)
        {
            Configuration = config;
        }

        public HttpConfiguration Configuration { get; set; }

        //// GET: MainPage/Main
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult About()
        {
            return View();
        }

        public ActionResult PasswordGenerator()
        {
            return View();
        }

        public ActionResult DeleteAccount()
        {
            return View();
        }

        public ActionResult Vault()
        {
           
            return View();
        }

        public ActionResult Configs()
        {

            return View();
        }

        public ActionResult Edit(int id)
        {
            using (var db = new dotNETDBEntities())
            {
                var pp = db.Pages.Where(p => p.PageID == id).Include("Passwords").First();
                return View(pp);
            }
        }


    }
}