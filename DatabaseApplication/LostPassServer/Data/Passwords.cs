//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace LostPassServer.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Passwords
    {
        [Required]
        [Display(Name = "Username")]
        public string UserLogin { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Pass { get; set; }
        public int PageID { get; set; }
        public int PasswordID { get; set; }
    
        public virtual Pages Pages { get; set; }
    }
}
