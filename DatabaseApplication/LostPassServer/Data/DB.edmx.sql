create database dotNETDB;
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/08/2016 19:12:25
-- Generated from EDMX file: C:\Users\User\Desktop\prokoplalka\Projects\lostpass_client\DatabaseApplication\LostPassServer\Data\DB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [dotNETDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__Passwords__PageI__15502E78]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Passwords] DROP CONSTRAINT [FK__Passwords__PageI__15502E78];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Configs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Configs];
GO
IF OBJECT_ID(N'[dbo].[Pages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Pages];
GO
IF OBJECT_ID(N'[dbo].[Passwords]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Passwords];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Configs'
CREATE TABLE [dbo].[Configs] (
    [Username] nvarchar(200)  NOT NULL,
    [ColorPalette] bit  NOT NULL,
    [HidePasswords] bit  NOT NULL,
    [SortOrder] varchar(50)  NOT NULL
);
GO

-- Creating table 'Pages'
CREATE TABLE [dbo].[Pages] (
    [PageID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(250)  NULL,
    [URL] nvarchar(max)  NULL,
    [Username] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Passwords'
CREATE TABLE [dbo].[Passwords] (
    [UserLogin] nvarchar(400)  NULL,
    [Pass] nvarchar(1000)  NULL,
    [PageID] int  NOT NULL,
    [PasswordID] int IDENTITY(1,1) NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Username] in table 'Configs'
ALTER TABLE [dbo].[Configs]
ADD CONSTRAINT [PK_Configs]
    PRIMARY KEY CLUSTERED ([Username] ASC);
GO

-- Creating primary key on [PageID] in table 'Pages'
ALTER TABLE [dbo].[Pages]
ADD CONSTRAINT [PK_Pages]
    PRIMARY KEY CLUSTERED ([PageID] ASC);
GO

-- Creating primary key on [PasswordID] in table 'Passwords'
ALTER TABLE [dbo].[Passwords]
ADD CONSTRAINT [PK_Passwords]
    PRIMARY KEY CLUSTERED ([PasswordID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PageID] in table 'Passwords'
ALTER TABLE [dbo].[Passwords]
ADD CONSTRAINT [FK__Passwords__PageI__15502E78]
    FOREIGN KEY ([PageID])
    REFERENCES [dbo].[Pages]
        ([PageID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__Passwords__PageI__15502E78'
CREATE INDEX [IX_FK__Passwords__PageI__15502E78]
ON [dbo].[Passwords]
    ([PageID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------