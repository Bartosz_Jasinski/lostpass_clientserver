﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Internal;
using DatabaseApplication.login;

namespace DatabaseApplication.Net
{
    public class HTTPClientConfig
    {
        static HttpClient CreateClient(string accessToken = "")
        {
            var client = new HttpClient();
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                client.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", accessToken);
            }
            return client;
        }

        public static HttpClient CreateClientForCurrentUser()
        {
            if (Login.Token.IsNullOrEmpty())
            {
                throw new ArgumentException("User not logged in");
            }
            return CreateClient(Login.Token);
        }
    }
}
