﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using DatabaseApplication.crypto;
using DatabaseApplication.io.entities;
using DatabaseApplication.Models;
using DatabaseApplication.Utils;
using LostPassServer.Data;
using Newtonsoft.Json;

namespace DatabaseApplication.Net
{
    public class HttpData
    {
        private string path;

        public HttpData()
        {
            path = ConfigurationManager.AppSettings["path"];
        }

        public async Task<List<PageDTO>> GetAllPages(HttpClient client)
        {
            if (client == null)
                throw new ArgumentException();
            try
            {
                var resp = await client.GetAsync(path + "/api/pages");
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return null;
                }
                return JsonConvert.DeserializeObject<List<PageDTO>>(res);
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return null;
            }
        }

        public async Task<string> AddPage(Page p, HttpClient client)
        {
            var byteContent = GetByteContent(p);
            try
            {
                var resp = await client.PostAsync(path + "/api/pages", byteContent);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode && resp.StatusCode != HttpStatusCode.InternalServerError)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return null;
                }
                return res;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return null;
            }
        }

        public async Task<bool> PasswordExists(int passwordId, HttpClient client)
        {
            var resp = await client.GetAsync(path + "/api/passwords/" + passwordId);
            if (resp.IsSuccessStatusCode)
                return true;
            if (resp.StatusCode == HttpStatusCode.NotFound)
                return false;
            throw new HttpResponseException(resp.StatusCode);
        }

        public Page GetPage(string url)
        {
            return null;
        }

        public async Task EditPage(Page page, HttpClient client)
        {
            var content = GetByteContent(page);
            try
            {
                var resp = await client.PutAsync(path + "/api/pages/" + page.PageID, content);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }

        public async Task DeletePage(int id, HttpClient client)
        {
            try
            {
                var resp = await client.DeleteAsync(path + "/api/pages/" + id);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }

        #region Helpers

        private static ByteArrayContent GetByteContent(object p)
        {
            var content = JsonConvert.SerializeObject(p);
            var buffer = System.Text.Encoding.UTF8.GetBytes(content);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return byteContent;
        }

        #endregion

        public async Task DeletePassword(int id, HttpClient client)
        {
            try
            {
                var resp = await client.DeleteAsync(path + "/api/passwords/" + id);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }

        public async Task AddPassword(Password p, HttpClient client)
        {
            var byteContent = GetByteContent(p);
            try
            {
                var resp = await client.PostAsync(path + "/api/passwords", byteContent);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode && resp.StatusCode != HttpStatusCode.InternalServerError)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }

        public async Task RecryptAll(string oldHash, HttpClient client)
        {
            var newHash = PasswordHash.Key;
            var Pages = await GetAllPages(client);
            PasswordHash.Key = oldHash;
            foreach (var page in Pages)
            {
                
                page.Passwords.ForEach(pass =>
                {
                    pass.Password = pass.Password.Decrypt();
                    pass.Login = pass.Login.Decrypt();
                });
            }
            PasswordHash.Key = newHash;
            foreach (var page in Pages)
            {
                foreach (var pass in page.Passwords)
                {
                    pass.Password = pass.Password.Encrypt();
                    pass.Login = pass.Login.Encrypt();
                    pass.PageID = page.PageID;
                    await EditPassword(pass, client);
                }
            }

        }

        private async Task EditPassword(PasswordDTO pass, HttpClient client)
        {
            var content = GetByteContent(new PasswordDTO()
            {
                Password = pass.Password,
                Login = pass.Login,
                PageID = pass.PageID,
                PasswordID = pass.PasswordID
            });
            try
            {
                var resp = await client.PutAsync(path + "/api/passwords/" + pass.PasswordID, content);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }

        public async Task<Configs> GetConfig(HttpClient client)
        {
            if (client == null)
                throw new ArgumentException();
            try
            {
                var resp = await client.GetAsync(path + "/api/configs");
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return null;
                }
                return JsonConvert.DeserializeObject<Configs>(res);
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return null;
            }
        }

        public async Task SyncConfigs(Configs conf, HttpClient client)
        {
            var content = GetByteContent(conf);
            try
            {
                var resp = await client.PutAsync(path + "/api/configs/" + conf.Username, content);
                var res = await resp.Content.ReadAsStringAsync();
                if (!resp.IsSuccessStatusCode)
                {
                    MessageProvider.Instance.SendMessage(res);
                    return;
                }
                return;
            }
            catch (HttpRequestException e)
            {
                MessageProvider.Instance.SendMessage(e.Message);
                return;
            }
        }
    }
}