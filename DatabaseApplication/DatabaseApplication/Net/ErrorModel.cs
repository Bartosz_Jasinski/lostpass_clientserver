﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace DatabaseApplication.Net
{
    public class ErrorModel
    {
        public string message;
        public Dictionary<string,string[]> modelState;
    }

    public class ModelState
    {
        [JsonProperty("")]
        public string[] _;
    }
}
