﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DatabaseApplication.Annotations;
using DatabaseApplication.crypto;
using DatabaseApplication.io;
using DatabaseApplication.io.entities;


namespace DatabaseApplication.Utils
{
    /// <summary>
    /// Klasa do różnych fajnych rzeczy
    /// </summary>
    public class PasswordAnalyser
    {
        private readonly ICollection<Page> pages;


        ///<summary>
        /// Procent użycia hasła przy którym powiadowiamy użytkownika
        /// </summary>
        private const double DangerousPassUsageRate = 0.3;

        public PasswordAnalyser(ICollection<Page> pages)
        {
            this.pages = pages;
        }


        /// <summary>
        /// Checks if any password is used in more than 30% cases
        /// If so, sends message of Other type.
        /// Check works only if user has more than 10 passwords saved
        /// </summary>
        /// <returns></returns>
        public bool CheckIfAnyPasswordUsedToOften()
        {
            var passes = pages.SelectMany(page => page.Passwords)
                .GroupBy(s => s.Pass.Decrypt())
                .Select(group => new {Cnt = group.Count(), Pass = group.Key})
                .OrderByDescending(i => i.Cnt);


            if (passes.Sum(arg => arg.Cnt) > 10)
            {
                if (passes.First().Cnt > DangerousPassUsageRate*passes.Sum(arg => arg.Cnt))
                {
                    //TODO FindResource nie działa, nie wiem czemu
                    //var str = (string) Application.Current.TryFindResource("PasswordTooFrequent");
                    MessageProvider.Instance.SendMessage(
                        new Message(
                            string.Format("Password \'{0}\' has been used {1} times. It's {2}% of used passwords!",
                                passes.First().Pass, passes.First().Cnt,
                                passes.First().Cnt*100/passes.Sum(arg => arg.Cnt))));
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Metoda sprawdza jakość hasła
        /// </summary>
        /// <returns>Double of 0 do 100</returns>
        /// <remarks>Algorytm pożyszony stąd http://www.passwordmeter.com</remarks>
        public static int PasswordStrength(string password)
        {
            int res = 0;
            if (string.IsNullOrEmpty(password))
                return 0;
            int numberOfCharachters = password.Length;
            int uppercase = new Regex(@"\p{Lu}").Matches(password).Count;
            int lowercase = new Regex(@"\p{Ll}").Matches(password).Count;
            int digits = new Regex(@"\d").Matches(password).Count;
            int symbols = new Regex(@"\W").Matches(password).Count;
            int mnos = 0;
            if (password.Length > 1)
                mnos = new Regex(@"(\W|\d)").Matches(password.Substring(1, numberOfCharachters - 2)).Count;
            int req = (uppercase > 0 ? 1 : 0) + (lowercase > 0 ? 1 : 0) +
                      (digits > 0 ? 1 : 0) + (symbols > 0 ? 1 : 0);
            if (req >= 3)
            {
                if (numberOfCharachters >= 8)
                    req++;
            }
            else
            {
                req = 0;
            }

            bool lettersOnly = uppercase + lowercase == numberOfCharachters;
            bool numbersOnly = digits == numberOfCharachters;
            int consUpper = uppercase - new Regex(@"\p{Lu}{1,}").Matches(password).Count;
            int consLower = lowercase - new Regex(@"\p{Ll}{1,}").Matches(password).Count;
            int consDig = digits - new Regex(@"\d{1,}").Matches(password).Count;
            int seqLet = Math.Max(0, LongestCommonSubstring("ABCDEFGHIJKLMNOPQRSTUVWXYZ", password.ToUpper()) - 2);
            int seqDig = Math.Max(0, LongestCommonSubstring("0123456789", password) - 2);
            int seqSym = Math.Max(0, LongestCommonSubstring("!@#$%^&*()_+", password) - 2);
            res = numberOfCharachters*4 + (uppercase > 0 ? (numberOfCharachters - uppercase)*2 : 0) +
                  (lowercase > 0 ? (numberOfCharachters - lowercase)*2 : 0) + (lowercase + uppercase > 0 ? digits*4 : 0) +
                  symbols*6 + mnos*2 + req*2 - (lettersOnly
                      ? numberOfCharachters
                      : 0) - (numbersOnly
                      ? numberOfCharachters
                      : 0) -
                  consUpper*2 - consLower*2 - consDig*2 -
                  seqLet*3 - seqSym*3 - seqDig*3;
            return Math.Max(0, Math.Min(100, res));
        }

        private static int LongestCommonSubstring(string str1, string str2)
        {
            int[,] l = new int[str1.Length, str2.Length];
            int lcs = -1;

            int end = -1;

            for (int i = 0; i < str1.Length; i++)
            {
                for (int j = 0; j < str2.Length; j++)
                {
                    if (str1[i] == str2[j])
                    {
                        if (i == 0 || j == 0)
                        {
                            l[i, j] = 1;
                        }
                        else
                            l[i, j] = l[i - 1, j - 1] + 1;
                        if (l[i, j] > lcs)
                        {
                            lcs = l[i, j];
                            end = i;
                        }
                    }
                    else
                        l[i, j] = 0;
                }
            }
            return lcs;
        }
    }
}