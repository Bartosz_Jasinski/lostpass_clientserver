﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using DatabaseApplication.gui.Main.Settings;
using DatabaseApplication.Net;
using LostPassServer.Data;

namespace DatabaseApplication.Utils
{
    public static class UserConfigs
    {
        public static Configs Settings { get; set; }

        public static async Task GetUserConfigs(HttpClient client)
        {
            Settings = await new HttpData().GetConfig(client);
            if (Settings == null)
            {
                Settings = new Configs()
                {
                    SortOrder = "Name - ascending"
                };
            }
            SettingsChanged();
        }

        public static async Task Sync()
        {
            using (var client = HTTPClientConfig.CreateClientForCurrentUser())
            {
                await new HttpData().SyncConfigs(Settings, client);
            }
        }

        public static event Action SettingsChanged;
    }
}
