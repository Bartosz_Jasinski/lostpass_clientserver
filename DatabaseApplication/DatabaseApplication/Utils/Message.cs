﻿namespace DatabaseApplication.Utils
{
    public enum MessageType
    {
        LoginError, ValidationError, Other
    }
    public class Message
    {
        public string Data { get; set; }
        public Message(string data, MessageType type = MessageType.Other)
        {
            Data = data;
            Type = type;
        }

        public MessageType Type { get; set; }

        public override string ToString()
        {
            return Data;
        }
    }
}