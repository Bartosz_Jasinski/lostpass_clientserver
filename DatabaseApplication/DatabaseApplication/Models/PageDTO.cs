﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Internal;
using DatabaseApplication.io.entities;

namespace DatabaseApplication.Models
{
    public class PageDTO
    {
        public int PageID { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string Username { get; set; }

        public List<PasswordDTO> Passwords { get; set; }

        public Page ToPage()
        {
            return new Page()
            {
                PageID = this.PageID,
                Name = this.Name,
                Username = this.Username,
                URL = this.URL,
                Passwords = this.Passwords.Select(dto => dto.ToPassword(PageID)).ToList()
            };
        }
    }
}
