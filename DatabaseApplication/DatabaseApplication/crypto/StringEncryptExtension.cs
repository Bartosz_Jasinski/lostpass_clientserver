﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.crypto
{
    public static class StringCryptExtension
    {
        public static string Encrypt(this string s)
        {
            var enc = new AesEncryptor();
            return enc.EncryptText(s, PasswordHash.Key);
        }

        public static string Decrypt(this string s)
        {
            var enc = new AesEncryptor();
            return enc.DecryptText(s, PasswordHash.Key);
        }
    }
}
