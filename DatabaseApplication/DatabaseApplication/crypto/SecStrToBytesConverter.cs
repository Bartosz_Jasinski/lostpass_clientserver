﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseApplication.crypto
{
    class SecStrToBytesConverter
    {
        /// <summary>
        /// Converts SecureString to Byte Array
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// 
        public static byte[] GetBytes(SecureString secureString, Encoding encoding=null)
        {
            if (secureString == null)
            {
                throw new ArgumentNullException(nameof(secureString));
            }

            encoding = encoding ?? Encoding.UTF8;

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(secureString);
                return encoding.GetBytes(Marshal.PtrToStringUni(unmanagedString));
            }
            catch (Exception e)
            {
                if (unmanagedString != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(unmanagedString);
                }
                return null;
            }

        }
    }
}
