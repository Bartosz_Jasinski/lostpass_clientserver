﻿namespace DatabaseApplication.crypto
{
    public interface IEncryptor
    {
        /// <summary>
        /// Metoda do szyfrowania tekstu
        /// </summary>
        /// <param name="input">To co szyfrujemy</param>
        /// <param name="password">Klucz do szyfrowania</param>
        /// <returns>Zaszyfrowany tekst w base64</returns>
        string EncryptText(string input, string password);

        /// <summary>
        /// Metoda do szyfrowania tekstu
        /// </summary>
        /// <param name="input">To co szyfrujemy w base64</param>
        /// <param name="password">Klucz do szyfrowania</param>
        /// <returns>Odszyfrowany tekst</returns>
        string DecryptText(string input, string password);
    }
}