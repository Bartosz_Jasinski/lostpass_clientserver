﻿using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using Castle.Core.Internal;

namespace DatabaseApplication.crypto
{
    public class PasswordHash
    {
        public static string Key { get; set; }
        public static string GetPasswordHash(SecureString pass)
        {
            if (pass == null || pass.Length == 0)
                return "";
            var bytes = SecStrToBytesConverter.GetBytes(pass);
            var enc = new Rfc2898DeriveBytes(bytes, new AesEncryptor().saltBytes, 4141);
            Array.Clear(bytes, 0, bytes.Length);
            return Encoding.ASCII.GetString(enc.GetBytes(100));
        }
    }
}