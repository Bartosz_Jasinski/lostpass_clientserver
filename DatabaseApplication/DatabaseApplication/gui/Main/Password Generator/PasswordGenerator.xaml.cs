﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DatabaseApplication.gui.Main.Password_Generator
{
    /// <summary>
    /// Interaction logic for PasswordGenerator.xaml
    /// </summary>
    public partial class PasswordGenerator : UserControl
    {
        public PasswordGenerator()
        {
            InitializeComponent();
        }

        private PasswordGeneratorViewModel Context => DataContext as PasswordGeneratorViewModel;

        private void CopyClick(object sender, RoutedEventArgs e)
        {
            Clipboard.SetDataObject(Context.GeneratedPassword); 
        }
    }
}