﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using DatabaseApplication.Annotations;
using DatabaseApplication.password;
using Generator = DatabaseApplication.password.PasswordGenerator;

namespace DatabaseApplication.gui.Main.Password_Generator
{
    class PasswordGeneratorViewModel : INotifyPropertyChanged
    {
        private bool _capital;
        private bool _small;
        private bool _digits;
        private bool _symbols;
        private string _length;
        private string _generatedPassword;

        public PasswordGeneratorViewModel()
        {
            LongListToTestComboVirtualization = Enumerable.Range(3, 50).ToList();
            GenCommand=new SimpleCommand(GenPass);
            Length = "12";
            Small = true;
            Capital = true;
            Digits = true;
            GenPass();
        }

        public string Length
        {
            get { return _length; }
            set
            {
                _length = value;
                GenPass();
            }
        }

        public IList<int> LongListToTestComboVirtualization { get; }

        public string GeneratedPassword
        {
            get { return _generatedPassword; }
            set
            {
                _generatedPassword = value;
                OnPropertyChanged();
            }
        }

        public bool Capital
        {
            get { return _capital; }
            set
            {
                _capital = value;
                GenPass();
            }
        }

        public bool Small
        {
            get { return _small; }
            set
            {
                _small = value;
                GenPass();
            }
        }

        public bool Digits
        {
            get { return _digits; }
            set
            {
                _digits = value;
                GenPass();
            }
        }

        public bool Symbols
        {
            get { return _symbols; }
            set
            {
                _symbols = value;
                GenPass();
            }
        }

        public ICommand GenCommand { get; set; }

        public void GenPass()
        {
            Generator gen = null ;
            try
            {
                gen = new Generator(int.Parse(Length));
            }
            catch (FormatException)
            {
                return;
            }
            if (Capital)
                gen = new CapitalLettersPasswordGeneratorDecorator(gen);
            if (Small)
                gen = new SmallLettersPasswordGeneratorDecorator(gen);
            if (Digits)
                gen = new DigitsPasswordGeneratorDecorator(gen);
            if (Symbols)
                gen = new SymbolsPasswordGeneratorDecorator(gen);
            GeneratedPassword = gen.Generate();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
