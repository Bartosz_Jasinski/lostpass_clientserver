﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using DatabaseApplication.io.entities;

using DatabaseApplication.Utils;
using MaterialDesignThemes.Wpf;
using Page = DatabaseApplication.io.entities.Page;

namespace DatabaseApplication.gui.Main.Vault
{
    /// <summary>
    /// Interaction logic for Vault.xaml
    /// </summary>
    public partial class Vault : UserControl
    {
        public Vault()
        {
            InitializeComponent();
        }


        private async void DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventargs)
        {
            if (!((bool)eventargs.Parameter))
                return;
            var context = DataContext as MainWindowViewModel;
            if (context == null)
                return;
            var b = await context.OnPasswordAdded();
            if (!b)
            {
                eventargs.Cancel();
            }
        }

        private void DialogHost_OnDialogOpened(object sender, DialogOpenedEventArgs eventargs)
        {
            var context = DataContext as MainWindowViewModel;
            if (context == null)
                return;
            context.DialogMessage = "";
        }
    }
}