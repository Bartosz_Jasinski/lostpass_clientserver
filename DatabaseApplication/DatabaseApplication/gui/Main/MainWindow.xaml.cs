﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using DatabaseApplication.gui.Main.Vault;
using DatabaseApplication.io;
using DatabaseApplication.io.entities;

using DatabaseApplication.login;
using DatabaseApplication.Utils;
using MahApps.Metro.Controls;
using MaterialDesignThemes.Wpf;
using Page = DatabaseApplication.io.entities.Page;

namespace DatabaseApplication.gui.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
           

        }

        
        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var dependencyObject = Mouse.Captured as DependencyObject;
            while (dependencyObject != null)
            {
                if (dependencyObject is ScrollBar) return;
                dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            MenuToggleButton.IsChecked = false;
        }

        private void SecondListBoxClick(object sender, MouseButtonEventArgs e)
        {
            var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
            if (item != null)
            {
                if (((StackPanel)item.DataContext).Name == "Exit")
                {
                    Application.Current.Shutdown();
                }
                if (((StackPanel)item.DataContext).Name == "Logout")
                {
                    Login.Logout();
                    OpenStartWindow();
                }
            }
        }
        private void OpenStartWindow()
        {
            
            DatabaseApplication.StartWindow.StartWindow mw = new DatabaseApplication.StartWindow.StartWindow();
            mw.Height = this.Height;
            mw.Width = this.Width;
            mw.Left = this.Left;
            mw.Top = this.Top;
            mw.WindowState = this.WindowState;
            this.Close();
            mw.Show();
        }

        private void SecondListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var item = ItemsControl.ContainerFromElement(sender as ListBox, e.OriginalSource as DependencyObject) as ListBoxItem;
                if (item != null)
                {
                    if (((StackPanel)item.DataContext).Name == "Exit")
                    {
                        Application.Current.Shutdown();
                    }
                    if (((StackPanel)item.DataContext).Name == "Logout")
                    {
                        Login.Logout();
                        OpenStartWindow();
                    }
                }
            }
            if (e.Key == Key.Escape)
            {
                MenuToggleButton.IsChecked = false;
            }
        }

        private void FirstListBoxKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Escape)
                MenuToggleButton.IsChecked = false;
        }
    }
}

