﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseApplication.crypto;
using DatabaseApplication.io.entities;
using DatabaseApplication.login;
using DatabaseApplication.StartWindow;
using DatabaseApplication.Utils;
using MaterialDesignThemes.Wpf;

namespace DatabaseApplication.gui.StartWindow
{
    /// <summary>
    /// Interaction logic for RegisterUC.xaml
    /// </summary>
    public partial class RegisterUC : UserControl
    {
        public RegisterUC()
        {
            InitializeComponent();
            Loaded += (sender, e) =>
                    MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private async void SignupButton_OnClick(object sender, RoutedEventArgs e)
        {
            var login = DataContext as LoginContext;
            if (PasswordTB.Password != PasswordConfTB.Password)
            {
                MessageProvider.Instance.SendMessage("Paswords do not match");
                return;
            }

            UserTB.Text = UserTB.Text.Trim();
            if(!DatabaseApplication.login.Login.IsEmailValid(UserTB.Text))
            {
                MessageProvider.Instance.SendMessage("Email is not valid");
                return;
            }

            try
            {
                var signed = await Login.CreateNewUser(UserTB.Text, PasswordTB.Password);
                if (signed==null)
                    return;
                
                await DialogHost.Show(this.DialogHost.DialogContent);
                ((DatabaseApplication.StartWindow.StartWindow) Window.GetWindow(this)).ContentControl.Content =
                    new LoginUC();
            }
            catch (Exception)
            {
                MessageProvider.Instance.SendMessage("User already exists");
            }
        }

        private void LogInButtonClick(object sender, RoutedEventArgs e)
        {
            var newContent = new LoginUC();
            newContent.UserTB.Text = UserTB.Text;
            newContent.PasswordTB.Password = PasswordTB.Password;
            ((DatabaseApplication.StartWindow.StartWindow)Window.GetWindow(this)).ContentControl.Content = newContent;
        }
    }
}
