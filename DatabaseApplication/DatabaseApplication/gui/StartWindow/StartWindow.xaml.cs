﻿using System;
using System.Windows;
using System.Windows.Input;
using Castle.Core.Internal;
using DatabaseApplication.crypto;
using DatabaseApplication.gui.Main;
using DatabaseApplication.gui.Main.Vault;
using DatabaseApplication.gui.StartWindow;
using DatabaseApplication.io.entities;
using DatabaseApplication.login;
using DatabaseApplication.Utils;
using MahApps.Metro.Controls;

namespace DatabaseApplication.StartWindow
{
    /// <summary>
    /// Interaction logic for StartWindow.xaml
    /// </summary>
    public partial class StartWindow : MetroWindow
    {
        public bool PasswordChanged = false;
        public string OldHash = null;
        public StartWindow()
        {
            InitializeComponent();
            var u = (DataContext as LoginContext).CheckRememberedLogin();
            if (!u.IsNullOrEmpty())
            {
                Login.Token = u;
                PasswordHash.Key = SettingsProvider.ReadSetting(Properties.Settings.Default.RememberEmail);
                OpenMainWindow();
            }
            Loaded += (sender, e) =>
                    MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        /*private async void LogInButtonClick(object sender, RoutedEventArgs e)
        {
            var login = DataContext as LoginContext;
            login.Password = PasswordTB.SecurePassword;

            var usr = new User()
            {
                UserLogin = login.UserName,
                PassHash = PasswordHash.GetPasswordHash(login.Password)
            };
            LoginButton.IsEnabled = false;
            SignupButton.IsEnabled = false;
            var u = await Login.Authenticate(login.UserName, PasswordTB.Password);
            LoginButton.IsEnabled = true;
            SignupButton.IsEnabled = true;
            if (u == null)
                return;
            //login.RememberUser(u);
            OpenMainWindow(null);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var login = DataContext as LoginContext;
            login.Password = PasswordTB.SecurePassword;

            var usr = new User()
            {
                UserLogin = login.UserName,
                PassHash = PasswordHash.GetPasswordHash(login.Password)
            };
            try
            {
                var signed = Login.CreateNewUser(usr.UserLogin, PasswordTB.Password);
                //OpenMainWindow(signed);
            }
            catch (Exception)
            {
                MessageProvider.Instance.SendMessage("User already exists");
            }
        }*/

        private async void OpenMainWindow()
        {
            MainWindow mw = new MainWindow();

            mw.Height = this.Height;
            mw.Width = this.Width;
            mw.Left = this.Left;
            mw.Top = this.Top;
            mw.WindowState = this.WindowState;
            this.Close();
            this.ContentControl.Content = null;
            mw.Show();
            //await ((MainWindowViewModel)mw.DataContext).SetUser(usr);
        }


    }
}